package com.jykim.custom;

import com.jykim.custom.structure.queue.CustomArrayQueue;
import com.jykim.custom.structure.queue.CustomQueue;
import com.jykim.custom.structure.stack.CustomArrayStack;
import com.jykim.custom.structure.stack.CustomStack;

public class TestMain {
    public static void main(String[] args) {
        System.out.println("#### STACK ####");
        CustomStack<Integer> customStack = new CustomArrayStack<>();
        customStack.push(1);
        customStack.push(2);
        customStack.push(3);
        customStack.push(4);
        customStack.push(5);
//        customStack.push("a");        // generic type error

        System.out.println("empty : "+customStack.empty());

        System.out.println("size : " + customStack.size());
        System.out.println("TOP : " + customStack.top());
        System.out.println(customStack.toString());

        System.out.println("pop : " + customStack.pop());
        System.out.println("TOP : " + customStack.top());
        System.out.println(customStack.toString());

        System.out.println();
        System.out.println("#### QUEUE ####");

        CustomQueue<String> customQueue = new CustomArrayQueue<>();
        customQueue.push("a");
        customQueue.push("b");
        customQueue.push("c");
        customQueue.push("d");
        customQueue.push("e");
//        customQueue.push(1);          // generic type error

        System.out.println("empty : "+customQueue.empty());

        System.out.println("size : " + customQueue.size());
        System.out.println(customQueue.toString());

        System.out.println("pop : " + customQueue.pop());
        System.out.println(customQueue.toString());


    }

}
