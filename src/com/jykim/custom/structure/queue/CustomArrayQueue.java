package com.jykim.custom.structure.queue;

import com.jykim.custom.structure.CommonArrayUtil;

public class CustomArrayQueue<T> extends CommonArrayUtil<T> implements CustomQueue<T> {

    @SuppressWarnings("unchecked")
    public CustomArrayQueue() {
        super.objects = (T[]) new Object[0];
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object pop() {
        Object firstElement = super.objects[0];
        // todo 매번 임시객체를 생성하여 처리하면 비효율적이므로 개선 필요
        // todo 배열의 인덱스만 조정해주는 것은 어떠한가(@psw)
        Object[] tempObjects = new Object[super.size-1];
        System.arraycopy(super.objects, 1, tempObjects, 0, super.size-1);
        super.size--;
        super.objects = (T[]) tempObjects;
        return firstElement;
    }

    @Override
    public Object front() {
        return super.objects[0];
    }

    @Override
    public Object back() {
        return super.objects[super.size-1];
    }
}
