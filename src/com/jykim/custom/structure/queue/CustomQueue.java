package com.jykim.custom.structure.queue;

import com.jykim.custom.structure.BaseDataStructure;

public interface CustomQueue<T> extends BaseDataStructure<T> {
    Object pop();
    Object front();
    Object back();
}
