package com.jykim.custom.structure;

public interface BaseDataStructure<T> {

    void push(T t);

    boolean empty();

    int size();

    Object get(int i);
}
