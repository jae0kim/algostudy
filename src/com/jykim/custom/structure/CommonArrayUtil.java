package com.jykim.custom.structure;

public class CommonArrayUtil<T> implements BaseDataStructure<T> {

    protected T[] objects;
    protected int size = 0;
    private static final int ARRAY_MAX_SIZE = Integer.MAX_VALUE - 8;

    @SuppressWarnings("unchecked")
    @Override
    public void push(T obj) {
        
        if (this.size == ARRAY_MAX_SIZE) {
            throw new OutOfMemoryError("Array size over : current array size["+this.size+"]");
        }
        T[] tempObjects = (T[]) new Object[size+1];
        System.arraycopy(this.objects, 0, tempObjects, 0, this.objects.length);
        tempObjects[tempObjects.length-1] = obj;
        size++;
        this.objects = tempObjects;
    }

    @Override
    public boolean empty() {
        return this.objects.length == 0;
    }

    @Override
    public int size() {
        return this.objects.length;
    }

    @Override
    public Object get(int i) {
        return this.objects[i];
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder("[");
        for (Object ojb : this.objects) {
            result.append(String.valueOf(ojb)).append(", ");
        }

        return result.substring(0, result.length()-2) + "]";
    }
}
