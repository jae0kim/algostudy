package com.jykim.custom.structure.stack;

import com.jykim.custom.structure.BaseDataStructure;

public interface CustomStack<T> extends BaseDataStructure<T> {
    Object pop();
    Object top();
}
