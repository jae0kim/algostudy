package com.jykim.custom.structure.stack;

import com.jykim.custom.structure.CommonArrayUtil;

public class CustomArrayStack<T> extends CommonArrayUtil<T> implements CustomStack<T>{

    @SuppressWarnings("unchecked")
    public CustomArrayStack() {
        super.objects = (T[]) new Object[0];
    }

    @Override
    public Object top() {
        return this.objects[this.size-1];
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object pop() {
        Object lastElemet = this.objects[super.size-1];
        // todo 매번 임시객체를 생성하여 처리하면 비효율적이므로 개선 필요
        Object[] tempObjects = new Object[super.size-1];
        System.arraycopy(this.objects, 0, tempObjects, 0, this.objects.length-1);
        super.size--;
        this.objects = (T[]) tempObjects;
        return lastElemet;
    }



}
